(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["modules-settings-settings-module"],{

/***/ "/h6M":
/*!******************************************************************************!*\
  !*** ./src/app/modules/settings/components/incr-decr/incr-decr.component.ts ***!
  \******************************************************************************/
/*! exports provided: IncrDecrComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncrDecrComponent", function() { return IncrDecrComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _constants_servo_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../constants/servo.constants */ "GK5o");



class IncrDecrComponent {
    constructor() {
        this.changeValue = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.currentValue = 0;
    }
    increment() {
        if (this.currentValue < _constants_servo_constants__WEBPACK_IMPORTED_MODULE_1__["MAX_VALUE"]) {
            this.currentValue += _constants_servo_constants__WEBPACK_IMPORTED_MODULE_1__["STEP_DEGREE"];
            this.changeValue.next(this.currentValue);
        }
    }
    decrement() {
        if (this.currentValue > _constants_servo_constants__WEBPACK_IMPORTED_MODULE_1__["MIN_VALUE"]) {
            this.currentValue -= _constants_servo_constants__WEBPACK_IMPORTED_MODULE_1__["STEP_DEGREE"];
            this.changeValue.next(this.currentValue);
        }
    }
}
IncrDecrComponent.ɵfac = function IncrDecrComponent_Factory(t) { return new (t || IncrDecrComponent)(); };
IncrDecrComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: IncrDecrComponent, selectors: [["app-incr-decr"]], inputs: { title: "title", min: "min", max: "max" }, outputs: { changeValue: "changeValue" }, decls: 11, vars: 2, consts: [[1, "content"], [1, "title"], [1, "controls"], [1, "buttons"], [1, "button", 3, "click"], [1, "value"]], template: function IncrDecrComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function IncrDecrComponent_Template_div_click_5_listener() { return ctx.decrement(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "\u00AB");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function IncrDecrComponent_Template_div_click_7_listener() { return ctx.increment(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "\u00BB");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.currentValue, " ");
    } }, styles: ["[_nghost-%COMP%] {\n  width: 100%;\n}\n\n.content[_ngcontent-%COMP%] {\n  width: 100%;\n  border-radius: 5px;\n  margin-bottom: 20px;\n  box-sizing: border-box;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n\n.content[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  font-size: 18px;\n  color: white;\n  padding: 0 4px;\n  min-width: 40%;\n  margin-right: 30px;\n}\n\n.content[_ngcontent-%COMP%]   .controls[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n\n.content[_ngcontent-%COMP%]   .controls[_ngcontent-%COMP%]   .buttons[_ngcontent-%COMP%] {\n  width: 40%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n\n.content[_ngcontent-%COMP%]   .controls[_ngcontent-%COMP%]   .value[_ngcontent-%COMP%] {\n  color: white;\n  font-weight: bold;\n  font-size: 22px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2luY3ItZGVjci5jb21wb25lbnQuc2NzcyIsIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3RoZW1lcy9taXhpbnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLFdBQUE7QUFERjs7QUFJQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUNUQSxhQUFBO0VBQ0EsOEJEUzJCO0VDUjNCLG1CQUh1QztFQUl2QyxtQkFKMkQ7QURhN0Q7O0FBQUU7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFFSjs7QUFDRTtFQUNFLFdBQUE7RUNyQkYsYUFBQTtFQUNBLDhCRHFCNkI7RUNwQjdCLG1CQUh1QztFQUl2QyxtQkFKMkQ7QUQyQjdEOztBQUZJO0VBQ0UsVUFBQTtFQ3pCSixhQUFBO0VBQ0EsdUJBRnVCO0VBR3ZCLG1CQUh1QztFQUl2QyxtQkFKMkQ7QURrQzdEOztBQUpJO0VBQ0UsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQU1OIiwiZmlsZSI6ImluY3ItZGVjci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uL3RoZW1lcy9taXhpbnMuc2Nzcyc7XG5cbjpob3N0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jb250ZW50IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgQGluY2x1ZGUgZmxleE1peCgkanVzdGlmeTogc3BhY2UtYmV0d2Vlbik7XG5cbiAgLnRpdGxlIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBhZGRpbmc6IDAgNHB4O1xuICAgIG1pbi13aWR0aDogNDAlO1xuICAgIG1hcmdpbi1yaWdodDogMzBweDtcbiAgfVxuXG4gIC5jb250cm9scyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgQGluY2x1ZGUgZmxleE1peCgkanVzdGlmeTogc3BhY2UtYmV0d2Vlbik7XG5cbiAgICAuYnV0dG9ucyB7XG4gICAgICB3aWR0aDogNDAlO1xuICAgICAgQGluY2x1ZGUgZmxleE1peDtcbiAgICB9XG5cbiAgICAudmFsdWUge1xuICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICBmb250LXNpemU6IDIycHg7XG4gICAgfVxuICB9XG59XG4iLCJAbWl4aW4gZmxleE1peCgkanVzdGlmeTogY2VudGVyLCAkYWxpZ246IGNlbnRlciwgJGRpcmVjdGlvbjogcm93KSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogJGp1c3RpZnk7XG4gIGFsaWduLWl0ZW1zOiAkYWxpZ247XG4gIGZsZXgtZGlyZWN0aW9uOiAkZGlyZWN0aW9uO1xufVxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IncrDecrComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-incr-decr',
                templateUrl: './incr-decr.component.html',
                styleUrls: ['./incr-decr.component.scss']
            }]
    }], function () { return []; }, { title: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], min: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], max: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], changeValue: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "Aewg":
/*!***************************************!*\
  !*** ./src/app/pipes/seconds.pipe.ts ***!
  \***************************************/
/*! exports provided: SecondsPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecondsPipe", function() { return SecondsPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");


class SecondsPipe {
    transform(time) {
        return time / 10;
    }
}
SecondsPipe.ɵfac = function SecondsPipe_Factory(t) { return new (t || SecondsPipe)(); };
SecondsPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "appSecondsPipe", type: SecondsPipe, pure: true });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SecondsPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
                name: 'appSecondsPipe'
            }]
    }], null, null); })();


/***/ }),

/***/ "GK5o":
/*!***************************************************************!*\
  !*** ./src/app/modules/settings/constants/servo.constants.ts ***!
  \***************************************************************/
/*! exports provided: MIN_VALUE, MAX_VALUE, STEP_DEGREE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MIN_VALUE", function() { return MIN_VALUE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MAX_VALUE", function() { return MAX_VALUE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STEP_DEGREE", function() { return STEP_DEGREE; });
const MIN_VALUE = 0;
const MAX_VALUE = 180;
const STEP_DEGREE = 5;


/***/ }),

/***/ "HIKF":
/*!********************************************************************************!*\
  !*** ./src/app/modules/settings/components/start-stop/start-stop.component.ts ***!
  \********************************************************************************/
/*! exports provided: StartStopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StartStopComponent", function() { return StartStopComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_pipes_seconds_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/pipes/seconds.pipe */ "Aewg");



class StartStopComponent {
    constructor() {
        this.stateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.time = 0;
    }
    toggle() {
        this.isOn = !this.isOn;
        this.stateChange.next(this.isOn);
        if (this.isOn) {
            this.startTimer();
        }
        else {
            clearInterval(this.timer);
        }
    }
    startTimer() {
        clearInterval(this.timer);
        this.time = 0;
        this.timer = setInterval(() => this.time++, 100);
    }
    ngOnDestroy() {
        clearInterval(this.timer);
    }
}
StartStopComponent.ɵfac = function StartStopComponent_Factory(t) { return new (t || StartStopComponent)(); };
StartStopComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: StartStopComponent, selectors: [["app-start-stop"]], inputs: { title: "title" }, outputs: { stateChange: "stateChange" }, decls: 10, vars: 7, consts: [[1, "content"], [1, "title"], [1, "controls"], [1, "buttons"], [1, "button", 3, "click"], [1, "value"]], template: function StartStopComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function StartStopComponent_Template_div_click_5_listener() { return ctx.toggle(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "appSecondsPipe");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("button_active", ctx.isOn);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.isOn ? "OFF" : "ON");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](9, 5, ctx.time), " sec. ");
    } }, pipes: [src_app_pipes_seconds_pipe__WEBPACK_IMPORTED_MODULE_1__["SecondsPipe"]], styles: ["[_nghost-%COMP%] {\n  width: 100%;\n}\n\n.content[_ngcontent-%COMP%] {\n  width: 100%;\n  border-radius: 5px;\n  margin-bottom: 10px;\n  box-sizing: border-box;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n\n.content[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  font-size: 18px;\n  color: white;\n  padding: 0 4px;\n  min-width: 32%;\n  margin-right: 30px;\n}\n\n.content[_ngcontent-%COMP%]   .controls[_ngcontent-%COMP%] {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  flex-direction: row;\n}\n\n.content[_ngcontent-%COMP%]   .controls[_ngcontent-%COMP%]   .buttons[_ngcontent-%COMP%] {\n  width: 40%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n\n.content[_ngcontent-%COMP%]   .controls[_ngcontent-%COMP%]   .value[_ngcontent-%COMP%] {\n  color: white;\n  font-weight: bold;\n  font-size: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3N0YXJ0LXN0b3AuY29tcG9uZW50LnNjc3MiLCIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi90aGVtZXMvbWl4aW5zLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxXQUFBO0FBREY7O0FBSUE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VDVEEsYUFBQTtFQUNBLDhCRFMyQjtFQ1IzQixtQkFIdUM7RUFJdkMsbUJBSjJEO0FEYTdEOztBQUFFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBRUo7O0FBQ0U7RUFDRSxXQUFBO0VDckJGLGFBQUE7RUFDQSw4QkRxQjZCO0VDcEI3QixtQkFIdUM7RUFJdkMsbUJBSjJEO0FEMkI3RDs7QUFGSTtFQUNFLFVBQUE7RUN6QkosYUFBQTtFQUNBLHVCQUZ1QjtFQUd2QixtQkFIdUM7RUFJdkMsbUJBSjJEO0FEa0M3RDs7QUFKSTtFQUNFLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFNTiIsImZpbGUiOiJzdGFydC1zdG9wLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vdGhlbWVzL21peGlucy5zY3NzJztcblxuOmhvc3Qge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmNvbnRlbnQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBAaW5jbHVkZSBmbGV4TWl4KCRqdXN0aWZ5OiBzcGFjZS1iZXR3ZWVuKTtcblxuICAudGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogMCA0cHg7XG4gICAgbWluLXdpZHRoOiAzMiU7XG4gICAgbWFyZ2luLXJpZ2h0OiAzMHB4O1xuICB9XG5cbiAgLmNvbnRyb2xzIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBAaW5jbHVkZSBmbGV4TWl4KCRqdXN0aWZ5OiBzcGFjZS1iZXR3ZWVuKTtcblxuICAgIC5idXR0b25zIHtcbiAgICAgIHdpZHRoOiA0MCU7XG4gICAgICBAaW5jbHVkZSBmbGV4TWl4O1xuICAgIH1cblxuICAgIC52YWx1ZSB7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB9XG4gIH1cbn1cbiIsIkBtaXhpbiBmbGV4TWl4KCRqdXN0aWZ5OiBjZW50ZXIsICRhbGlnbjogY2VudGVyLCAkZGlyZWN0aW9uOiByb3cpIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiAkanVzdGlmeTtcbiAgYWxpZ24taXRlbXM6ICRhbGlnbjtcbiAgZmxleC1kaXJlY3Rpb246ICRkaXJlY3Rpb247XG59XG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](StartStopComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-start-stop',
                templateUrl: './start-stop.component.html',
                styleUrls: ['./start-stop.component.scss']
            }]
    }], function () { return []; }, { title: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], stateChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "I7x6":
/*!********************************************************!*\
  !*** ./src/app/modules/settings/settings.component.ts ***!
  \********************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var src_app_services_move_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/move.service */ "aSGA");
/* harmony import */ var _shared_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/components/navbar/navbar.component */ "8ifR");
/* harmony import */ var _components_incr_decr_incr_decr_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/incr-decr/incr-decr.component */ "/h6M");
/* harmony import */ var _components_start_stop_start_stop_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/start-stop/start-stop.component */ "HIKF");







class SettingsComponent {
    constructor(moveService) {
        this.moveService = moveService;
    }
    moveLeft(degree) {
        this.moveService.moveLeftServo(degree).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1)).subscribe();
    }
    moveRight(degree) {
        this.moveService.moveRightServo(degree).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1)).subscribe();
    }
    moveBase(degree) {
        this.moveService.moveBaseServo(degree).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1)).subscribe();
    }
}
SettingsComponent.ɵfac = function SettingsComponent_Factory(t) { return new (t || SettingsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_move_service__WEBPACK_IMPORTED_MODULE_2__["MoveService"])); };
SettingsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SettingsComponent, selectors: [["app-settings"]], decls: 6, vars: 0, consts: [[1, "settings"], ["title", "Horizontal Arm", 3, "changeValue"], ["title", "Vertical Arm", 3, "changeValue"], ["title", "Base Servo", 3, "changeValue"], ["title", "Pump"]], template: function SettingsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-incr-decr", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("changeValue", function SettingsComponent_Template_app_incr_decr_changeValue_2_listener($event) { return ctx.moveLeft($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "app-incr-decr", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("changeValue", function SettingsComponent_Template_app_incr_decr_changeValue_3_listener($event) { return ctx.moveRight($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "app-incr-decr", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("changeValue", function SettingsComponent_Template_app_incr_decr_changeValue_4_listener($event) { return ctx.moveBase($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-start-stop", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_shared_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_3__["NavbarComponent"], _components_incr_decr_incr_decr_component__WEBPACK_IMPORTED_MODULE_4__["IncrDecrComponent"], _components_start_stop_start_stop_component__WEBPACK_IMPORTED_MODULE_5__["StartStopComponent"]], styles: ["[_nghost-%COMP%] {\n  width: 100%;\n  height: 100%;\n}\n\n.settings[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  justify-content: flex-start;\n  align-items: flex-start;\n  flex-direction: column;\n  padding: 5%;\n  box-sizing: border-box;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NldHRpbmdzLmNvbXBvbmVudC5zY3NzIiwiLi4vLi4vLi4vLi4vLi4vLi4vLi4vdGhlbWVzL21peGlucy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFERjs7QUFJQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VDUkEsYUFBQTtFQUNBLDJCRFEyQjtFQ1AzQix1QkRPK0M7RUNOL0Msc0JETXVFO0VBQ3ZFLFdBQUE7RUFDQSxzQkFBQTtBQUVGIiwiZmlsZSI6InNldHRpbmdzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vdGhlbWVzL21peGlucy5zY3NzJztcblxuOmhvc3Qge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2V0dGluZ3Mge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBAaW5jbHVkZSBmbGV4TWl4KCRqdXN0aWZ5OiBmbGV4LXN0YXJ0LCAkYWxpZ246IGZsZXgtc3RhcnQsICRkaXJlY3Rpb246IGNvbHVtbik7XG4gIHBhZGRpbmc6IDUlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuIiwiQG1peGluIGZsZXhNaXgoJGp1c3RpZnk6IGNlbnRlciwgJGFsaWduOiBjZW50ZXIsICRkaXJlY3Rpb246IHJvdykge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6ICRqdXN0aWZ5O1xuICBhbGlnbi1pdGVtczogJGFsaWduO1xuICBmbGV4LWRpcmVjdGlvbjogJGRpcmVjdGlvbjtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SettingsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-settings',
                templateUrl: 'settings.component.html',
                styleUrls: ['settings.component.scss']
            }]
    }], function () { return [{ type: src_app_services_move_service__WEBPACK_IMPORTED_MODULE_2__["MoveService"] }]; }, null); })();


/***/ }),

/***/ "aSGA":
/*!******************************************!*\
  !*** ./src/app/services/move.service.ts ***!
  \******************************************/
/*! exports provided: MoveService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MoveService", function() { return MoveService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");



class MoveService {
    constructor(http) {
        this.http = http;
        // private readonly BASE_URL = 'http://192.168.4.74:3000/';
        this.BASE_URL = 'http://localhost:3000/';
    }
    moveLeftServo(degree) {
        console.log('BU: ', this.BASE_URL, degree);
        return this.http.get(this.BASE_URL.concat('left-servo'), { params: { position: degree.toString() } });
    }
    moveRightServo(degree) {
        return this.http.get(this.BASE_URL.concat('right-servo'), { params: { position: degree.toString() } });
    }
    moveBaseServo(degree) {
        return this.http.get(this.BASE_URL.concat('base-servo'), { params: { position: degree.toString() } });
    }
}
MoveService.ɵfac = function MoveService_Factory(t) { return new (t || MoveService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
MoveService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: MoveService, factory: MoveService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MoveService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "pU93":
/*!*****************************************************!*\
  !*** ./src/app/modules/settings/settings.module.ts ***!
  \*****************************************************/
/*! exports provided: SettingsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsModule", function() { return SettingsModule; });
/* harmony import */ var _shared_components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../../shared/components/navbar/navbar.module */ "BGWj");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _settings_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./settings.component */ "I7x6");
/* harmony import */ var _components_incr_decr_incr_decr_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/incr-decr/incr-decr.component */ "/h6M");
/* harmony import */ var _components_start_stop_start_stop_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/start-stop/start-stop.component */ "HIKF");
/* harmony import */ var src_app_pipes_seconds_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/pipes/seconds.pipe */ "Aewg");










class SettingsModule {
}
SettingsModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: SettingsModule });
SettingsModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ factory: function SettingsModule_Factory(t) { return new (t || SettingsModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _shared_components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_0__["NavbarModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild([
                {
                    path: '',
                    component: _settings_component__WEBPACK_IMPORTED_MODULE_4__["SettingsComponent"]
                }
            ])
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](SettingsModule, { declarations: [_settings_component__WEBPACK_IMPORTED_MODULE_4__["SettingsComponent"],
        _components_incr_decr_incr_decr_component__WEBPACK_IMPORTED_MODULE_5__["IncrDecrComponent"],
        _components_start_stop_start_stop_component__WEBPACK_IMPORTED_MODULE_6__["StartStopComponent"],
        src_app_pipes_seconds_pipe__WEBPACK_IMPORTED_MODULE_7__["SecondsPipe"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
        _shared_components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_0__["NavbarModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵsetClassMetadata"](SettingsModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"],
        args: [{
                declarations: [
                    _settings_component__WEBPACK_IMPORTED_MODULE_4__["SettingsComponent"],
                    _components_incr_decr_incr_decr_component__WEBPACK_IMPORTED_MODULE_5__["IncrDecrComponent"],
                    _components_start_stop_start_stop_component__WEBPACK_IMPORTED_MODULE_6__["StartStopComponent"],
                    src_app_pipes_seconds_pipe__WEBPACK_IMPORTED_MODULE_7__["SecondsPipe"]
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                    _shared_components_navbar_navbar_module__WEBPACK_IMPORTED_MODULE_0__["NavbarModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild([
                        {
                            path: '',
                            component: _settings_component__WEBPACK_IMPORTED_MODULE_4__["SettingsComponent"]
                        }
                    ])
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=modules-settings-settings-module.js.map